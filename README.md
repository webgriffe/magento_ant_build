Magento ANT Build
=================

Several ANT targets that make easy continuous integration tasks during Magento extension development.
Given a Magento extension these tasks allow the setup of a "clean" Magento installation on which will be installed the module itself and will be executed the test suite with EcomDev_PHPUnit.

Target
------

Implemented targets are:

* setup: installs Magento at specified version, configures the base URL, sets up admin password and installs the module.
* run-tests: installs EcomDev_PHPUnit and run the test suite.

Configuration variables (default.properties)
--------------------------------------------

The configuration variable in file `default.properties` are:

* `db_host`: hostname of MySQL server
* `db_user`: MySQL user to use for database connection
* `db_pass`: MySQL password to use for database connection
* `db_name`: MySQL database name (it will be also created another database with the same name plus the suffix `_test` for EcomDev_PHPUnit tests)
* `phpunit_filter`: the value for the option `--filter` for the PHPUnit command
* `base_url`: the base URL for the Magento installation
* `magento_relative_dir`: the path, relative to the module root, of the directory  in which Magento will be installed
* `install_sample_data`: whether to insall sample data

Requirements
------------

To successfully complete the build process it's required that on the machine are installed the following commands:

* `ant` 
* `n98-magerun`
* `modman`
* `phpunit`

The extension must be installable with `modman`; so it's required a `modman` file in the module root (for further informations see [https://github.com/colinmollenhour/modman](https://github.com/colinmollenhour/modman))

Usage
-----

Create/edit the `composer.json` file in the root of Magento extension, with the following content:

    {
    	[...]
        
        "require": {
            "webgriffe/magento_ant_build": "dev-master"
        },
        
        [...]
        
        "repositories": [
            {
                "type": "vcs",
                "url":  "git@bitbucket.org:webgriffe/magento_ant_build.git"
            }
        ]
    }

And install the dependency with:

	composer update webgriffe/magento_ant_build

Then copy the file `default.properties.dist` in the root directory of the module:

    cp vendor/webgriffe/magento_ant_build/default.properties.dist ./default.properties

and customize it according with your system configuration.
Link ANT target in the root directory of the module:

    ln -s vendor/webgriffe/magento_ant_build/build.xml build.xml

Finally it's possible run the ANT build with:

	ant -Dmagento_version=$VERSION
	
The list of installable Magento versions is the one of the `install` command of `n98-magerun` (see [https://github.com/netz98/n98-magerun#magento-installer](https://github.com/netz98/n98-magerun#magento-installer)).

